package testcases;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeSuite;
import pages.HowmanyPage;
import pages.LoginPage;
import pages.SearchPage;

/**
 * Created by Rahul on 8/4/2016.
 */
public class BaseTest {

    //WEB DRIVER DECLARATION
    protected WebDriver driver;

    //GETTING WEB URL FROM EXCEL FILE
    protected String BaseURL = UtilityTest.ReadData("GeneralData", 0, 1);

    //PAGE CLASS OBJECTS
    public LoginPage loginPage;
    public SearchPage searchPage;
    public HowmanyPage howmanyPage;
    public static DesiredCapabilities Caps;


    @BeforeSuite
    public void setup()
    {

        Caps = new DesiredCapabilities();
        Caps.setJavascriptEnabled(true);
        Caps.setCapability("takeScreenshot", false);

        //Caps.setCapability("phantomjs.binary.path", "E:\\Global_Automation\\phantomjs-2.1.1-windows\\bin\\phantomjs.exe");
        Caps.setCapability("phantomjs.binary.path", "/usr/local/share/applications/phantomjs-2.1.1-linux-x86_64/bin/phantomjs");
        Caps.setCapability("localToRemoteUrlAccessEnabled",true);
        Caps.setCapability("browserConnectionEnabled",true);
        driver = new PhantomJSDriver(Caps );
        driver.navigate().to(BaseURL);
        driver.manage().window().maximize();


      /*  System.setProperty("webdriver.chrome.driver","E:\\Global_Automation\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.navigate().to(BaseURL);*/
        
        loginPage = PageFactory.initElements(driver,LoginPage.class);
        searchPage = PageFactory.initElements(driver,SearchPage.class);
        howmanyPage = PageFactory.initElements(driver,HowmanyPage.class);


    }


}
