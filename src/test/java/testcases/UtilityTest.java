package testcases;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.testng.Reporter;
import org.testng.annotations.Test;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import static pages.HowmanyPage.*;
import static pages.SearchPage.*;
import static pages.SearchPage.NotFoundAnswers;
import static pages.SearchPage.SingleResult;



/**
 * Created by Rahul on 8/4/2016.
 */
public class UtilityTest {

    public static List<String> Queries;
    public static int counts=2;
    public static int Colnum=1;
    public static List<String> ActualAnswer1;
    public static List<String> ActualAnswer2;
    public static List<String> ActualAnswer3;
    public static List<String> ExpectedAnswer1;
    public static List<String> ExpectedAnswer2;
    public static List<String> ExpectedAnswer3;

    public static String XLSPath = "~/YappaTech_Data-Debug.xls";
    //F:\StatFuel_Debug\YappaTech_Data-Debug.xls
    ///usr/local/share/ConstantData/YappaTech_Data-Debug.xls
    ///var/lib/jenkins/workspace/Which_Debug/YappaTech_Data-Debug.xls


    @Test
    public static String ReadData(String SheetName, int ColNum, int RowNum) {


        //File file = new File(FilePath + "/" + FileName);

        //LOCAL PATH
        File file = new File(XLSPath);
        DataFormatter formatter = new DataFormatter();

        // File file = new File(FilePath + "\\" + FileName);


        FileInputStream inputStream = null;
        try {
            inputStream = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        Workbook Techbook = null;
        try {
            Techbook = new HSSFWorkbook(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }


        Sheet TechSheet = Techbook.getSheet(SheetName);
        int rowCount =TechSheet.getLastRowNum() - TechSheet.getFirstRowNum();

        // for (RowNum = 0; RowNum < rowCount + 1; RowNum++) {

        Row row = TechSheet.getRow(RowNum);

        //for ( ColNum = 0;  ColNum < row.getLastCellNum();  ColNum++) {
        //Print excel data in console

        Cell cell = row.getCell(ColNum);
        String value = formatter.formatCellValue(cell);

       /* row.getCell(ColNum).getStringCellValue();
          }
        System.out.println();
         }*/

        //return row.getCell(ColNum).getStringCellValue();
        return value;
    }


    //FUNCTION TO READ QUERY FROM EXCEL

    @Test
    public static List<String> ReadQueriesRows(String SheetName, int ColNum)
    {


        Queries = new ArrayList<String>();
        //File file = new File(FilePath + "/" + FileName);

        //LOCAL PATH
        File file = new File(XLSPath);
        DataFormatter formatter = new DataFormatter();

        // File file = new File(FilePath + "\\" + FileName);


        FileInputStream inputStream = null;
        try {
            inputStream = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        Workbook Techbook = null;
        try {
            Techbook = new HSSFWorkbook(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }


        Sheet TechSheet = Techbook.getSheet(SheetName);

        for (int RowNum = 2; RowNum <=TechSheet.getLastRowNum(); RowNum++) {

            Row row = TechSheet.getRow(RowNum);

            //for ( ColNum = 0;  ColNum < row.getLastCellNum();  ColNum++) {
            //Print excel data in console

            Cell cell = row.getCell(ColNum);
            Queries.add(formatter.formatCellValue(cell));


        }

        //return row.getCell(ColNum).getStringCellValue();
       /* for(int x=0;x<=Queries.size()-1;x++) {

            System.out.println(Queries.get(x).toString());
        }*/
        return Queries;
    }

    @Test
    public static List<String> WriteQueriesRows(String SheetName)
    {


        File file = new File(XLSPath);
        DataFormatter formatter = new DataFormatter();

        FileInputStream inputStream = null;
        try {
            inputStream = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        Workbook Techbook = null;


        try {
            Techbook = new HSSFWorkbook(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }


        Sheet TechSheet = Techbook.getSheet(SheetName);
        int rowCount =TechSheet.getLastRowNum() - TechSheet.getFirstRowNum();

        Row row = TechSheet.getRow(0);


        if(SingleResult.size()>0) {
            for (int j = 0; j < SingleResult.size(); j++) {

                //Fill data in row
                Row row1 = TechSheet.getRow(counts);
                if (row1.equals(null)) {
                    row1 = TechSheet.createRow(counts);

                }

                //row1.createCell(Colnum).setCellValue(AnswerPlayer.get(j).toString() + AnswerPosition.get(j).toString() + AnswerTeam.get(j).toString());
                row1.createCell(Colnum).setCellValue(SingleResult.get(j).toString());
                row1.createCell(Colnum+1).setCellValue("-");
                row1.createCell(Colnum+2).setCellValue("-");

                //counts++;
            }
        }
        if(MultipleAnswers1.size()>0)
        {
            for (int m1 = 0; m1 < MultipleAnswers1.size(); m1++) {

                Row row2 = TechSheet.getRow(counts);
                if (row2.equals(null)) {
                    row2 = TechSheet.createRow(counts);

                }

                row2.createCell(Colnum).setCellValue(MultipleAnswers1.get(m1).toString());

            }
        }
        if(MultipleAnswers2.size()>0)
        {
            for (int m2 = 0; m2 < MultipleAnswers2.size(); m2++) {

                Row row3 = TechSheet.getRow(counts);
                if (row3.equals(null)) {
                    row3 = TechSheet.createRow(counts);

                }
                row3.createCell(2).setCellValue(MultipleAnswers2.get(m2).toString());

            }
        }

        if(MultipleAnswers3.size()>0)
        {
            for (int m3 = 0; m3 < MultipleAnswers2.size(); m3++) {

                Row row4 = TechSheet.getRow(counts);
                if (row4.equals(null)) {
                    row4 = TechSheet.createRow(counts);

                }

                row4.createCell(3).setCellValue(MultipleAnswers3.get(m3).toString());

            }
        }
        else
        {

            for (int nfa = 0; nfa < NotFoundAnswers.size(); nfa++) {

                Row row5 = TechSheet.getRow(counts);
                if (row5.equals(null)) {
                    row5 = TechSheet.createRow(counts);

                }

                row5.createCell(Colnum).setCellValue(NotFoundAnswers.get(nfa).toString());
                row5.createCell(Colnum+1).setCellValue("-");
                row5.createCell(Colnum+2).setCellValue("-");

            }

        }
        counts++;


        //Close input stream

        try {
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream outputStream = null;

        try {
            outputStream = new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            Techbook.write(outputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return SingleResult;

    }

    @Test
    public static List<String> WriteQueriesRowsHowMany(String SheetName)
    {


        File file = new File(XLSPath);
        DataFormatter formatter = new DataFormatter();

        FileInputStream inputStream = null;
        try {
            inputStream = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        Workbook Techbook = null;


        try {
            Techbook = new HSSFWorkbook(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }


        Sheet TechSheet = Techbook.getSheet(SheetName);
        int rowCount =TechSheet.getLastRowNum() - TechSheet.getFirstRowNum();

        Row row = TechSheet.getRow(0);


        if(HSingleResult.size()>0) {
            for (int j = 0; j < HSingleResult.size(); j++) {

                //Fill data in row
                Row row1 = TechSheet.getRow(counts);
                if (row1.equals(null)) {
                    row1 = TechSheet.createRow(counts);

                }

                //row1.createCell(Colnum).setCellValue(AnswerPlayer.get(j).toString() + AnswerPosition.get(j).toString() + AnswerTeam.get(j).toString());
                row1.createCell(Colnum).setCellValue(HSingleResult.get(j).toString());
                row1.createCell(Colnum+1).setCellValue("-");
                row1.createCell(Colnum+2).setCellValue("-");

                //counts++;
            }
        }
        if(HMultipleAnswers1.size()>0)
        {
            for (int m1 = 0; m1 < HMultipleAnswers1.size(); m1++) {

                Row row2 = TechSheet.getRow(counts);
                if (row2.equals(null)) {
                    row2 = TechSheet.createRow(counts);

                }

                row2.createCell(Colnum).setCellValue(MultipleAnswers1.get(m1).toString());

            }
        }
        if(HMultipleAnswers2.size()>0)
        {
            for (int m2 = 0; m2 < HMultipleAnswers2.size(); m2++) {

                Row row3 = TechSheet.getRow(counts);
                if (row3.equals(null)) {
                    row3 = TechSheet.createRow(counts);

                }
                row3.createCell(2).setCellValue(HMultipleAnswers2.get(m2).toString());

            }
        }

        if(HMultipleAnswers3.size()>0)
        {
            for (int m3 = 0; m3 < HMultipleAnswers3.size(); m3++) {

                Row row4 = TechSheet.getRow(counts);
                if (row4.equals(null)) {
                    row4 = TechSheet.createRow(counts);

                }

                row4.createCell(3).setCellValue(HMultipleAnswers3.get(m3).toString());

            }
        }
        else
        {

            for (int nfa = 0; nfa < HNotFoundAnswers.size(); nfa++) {

                Row row5 = TechSheet.getRow(counts);
                if (row5.equals(null)) {
                    row5 = TechSheet.createRow(counts);

                }

                row5.createCell(Colnum).setCellValue(HNotFoundAnswers.get(nfa).toString());
                row5.createCell(Colnum+1).setCellValue("-");
                row5.createCell(Colnum+2).setCellValue("-");

            }

        }
        counts++;


        //Close input stream

        try {
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream outputStream = null;

        try {
            outputStream = new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            Techbook.write(outputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return HSingleResult;

    }




    @Test
    public static void CompareData(String SheetName)
    {

        ActualAnswer1 = new ArrayList<String>();
        ActualAnswer2 = new ArrayList<String>();
        ActualAnswer3 = new ArrayList<String>();
        ExpectedAnswer1 = new ArrayList<String>();
        ExpectedAnswer2 = new ArrayList<String>();
        ExpectedAnswer3 = new ArrayList<String>();

        File file = new File(XLSPath);
        DataFormatter formatter = new DataFormatter();

        FileInputStream inputStream = null;
        try {
            inputStream = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        Workbook Techbook = null;
        try {
            Techbook = new HSSFWorkbook(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }


        Sheet TechSheet = Techbook.getSheet(SheetName);


        for(int rownum=2;rownum<TechSheet.getLastRowNum()+1;rownum++) {

            Row row = TechSheet.getRow(rownum);
            Cell cell = row.getCell(Colnum);
            ActualAnswer1.add(cell.getStringCellValue());
        }
        for(int rownum=2;rownum<TechSheet.getLastRowNum()+1;rownum++) {

            Row row = TechSheet.getRow(rownum);
            Cell cell = row.getCell(Colnum+1);
            ActualAnswer2.add(cell.getStringCellValue());
        }
        for(int rownum=2;rownum<TechSheet.getLastRowNum()+1;rownum++) {

            Row row = TechSheet.getRow(rownum);
            Cell cell = row.getCell(Colnum+2);
            ActualAnswer3.add(cell.getStringCellValue());
        }


        for(int rownum=2;rownum<TechSheet.getLastRowNum()+1;rownum++) {

            Row row = TechSheet.getRow(rownum);
            Cell cell = row.getCell(Colnum+3);
            ExpectedAnswer1.add(cell.getStringCellValue());
        }for(int rownum=2;rownum<TechSheet.getLastRowNum()+1;rownum++) {

        Row row = TechSheet.getRow(rownum);
        Cell cell = row.getCell(Colnum+4);
        ExpectedAnswer2.add(cell.getStringCellValue());
    }for(int rownum=2;rownum<TechSheet.getLastRowNum()+1;rownum++) {

        Row row = TechSheet.getRow(rownum);
        Cell cell = row.getCell(Colnum+5);
        ExpectedAnswer3.add(cell.getStringCellValue());
    }

        int RowInc=2;
        int Pass=0;
        int Fail=0;

        for(int total=0;total<ActualAnswer1.size();total++)
        {


            if(ActualAnswer1.get(total).equals(ExpectedAnswer1.get(total)) && ActualAnswer2.get(total).equals(ExpectedAnswer2.get(total)) && ActualAnswer3.get(total).equals(ExpectedAnswer3.get(total)))
            {

                //System.out.println("PASS");
                Row row6 = TechSheet.getRow(RowInc);
               /* if (row6.equals(null)) {
                    row6 = TechSheet.createRow(counts);

                }*/

                row6.createCell(Colnum+6).setCellValue("Pass");
                RowInc++;
                Pass++;

            }
            else
            {
                //System.out.println("FAIL");


                Row row7 = TechSheet.getRow(RowInc);
                if (row7.equals(null)) {
                    row7 = TechSheet.createRow(RowInc);

                }

                row7.createCell(Colnum+6).setCellValue("Fail");
                RowInc++;
                Fail++;

            }


        }

        System.out.println("Total Number of Questions Tested :" + Queries.size());
        System.out.println("-------------------------------------------------------\n");
        System.out.println("Total Pass: "+ Pass);
        System.out.println("Total Fail: "+ Fail);

        Reporter.log("Total Number of Questions Tested :" + Queries.size());
        Reporter.log("-------------------------------------------------------\n");
        Reporter.log("Total Pass: "+ Pass);
        Reporter.log("Total Fail: "+ Fail);



        try {
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream outputStream = null;

        try {
            outputStream = new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            Techbook.write(outputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
