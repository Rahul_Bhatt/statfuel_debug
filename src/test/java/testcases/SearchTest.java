package testcases;

import org.testng.annotations.Test;

/**
 * Created by Rahul on 8/4/2016.
 */
public class SearchTest extends BaseTest {
    @Test(priority = 0)
    public void SearchByWhich()
    {

        //loginPage.Login(UtilityTest.ReadData("LoginData", 1, 1), UtilityTest.ReadData("LoginData", 1, 2));

        searchPage.InputWhichTypeQuery(UtilityTest.ReadQueriesRows("Which",0));

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        searchPage.CompareResult("Which");

    }
}


