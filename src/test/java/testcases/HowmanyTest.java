package testcases;

import org.testng.annotations.Test;

/**
 * Created by admin on 09-08-2016.
 */
public class HowmanyTest extends BaseTest {

    @Test(priority = 0)
    public void SearchByHowmany()
    {

        //loginPage.Login(UtilityTest.ReadData("LoginData", 1, 1), UtilityTest.ReadData("LoginData", 1, 2));

        howmanyPage.InputWhichTypeQuery(UtilityTest.ReadQueriesRows("HowMany",0));

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        howmanyPage.CompareResult("HowMany");

    }
}
