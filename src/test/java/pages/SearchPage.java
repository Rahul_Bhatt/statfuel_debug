package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import testcases.UtilityTest;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static testcases.UtilityTest.Queries;

/**
 * Created by Rahul on 8/4/2016.
 */
public class SearchPage {
    protected WebDriver driver;

    public SearchPage(WebDriver driver) {
        this.driver = driver;
    }



    @FindBy(id = "stat-search-lgd")
    private WebElement TxtboxSearch;

    @FindBy(id = "statefulsearch-form-guest")
    private WebElement BtnSearch;

    @FindBy(xpath = "//p[text()='No results found for the given criteria']")
    private List<WebElement> SectionNoResultFound;

    @FindBy(xpath = "//i[@class='fa fa-times']")
    private List<WebElement> BtnResultClose;

    @FindBy(tagName = "h4")
    private List<WebElement> Question;

    @FindBy(xpath = "//div[@class='row sa-ans-row']")
    private List<WebElement> AnswersSize;

    @FindBy(xpath = "//div[@class='wrap-ans single-ans']")
    private List<WebElement> SingleAnswer;

    @FindBy(xpath = "//span[@class='player']")
    private List<WebElement> PlayerDiv;

    @FindBy(xpath = "//span[@class='position']")
    private List<WebElement> PositionDiv;

    @FindBy(xpath = "//p[@class='team']")
    private List<WebElement> TeamDiv;

    @FindBy(xpath = "//span[@class='team']")
    private List<WebElement> TeamDivMultiple;

    @FindBy(xpath = "//p[@class='ans']")
    private List<WebElement> AnswerCountDiv;

    @FindBy(xpath = "//p[@class='ans superlative']")
    private List<WebElement> AnswerDivMultiple;


    public static List<String> NotFoundAnswers;
    public static List<String> SingleResult;
    public static List<String> Answer1;
    public static List<String> Answer2;
    public static List<String> Answer3;
    public static List<String> MultipleAnswers1;
    public static List<String> MultipleAnswers2;
    public static List<String> MultipleAnswers3;
    String MultipleData1="";
    String MultipleData2="";
    String MultipleData3="";



    public SearchPage InputWhichTypeQuery(List<String> Query)
    {

        SingleResult = new ArrayList<String>();
        Answer1 = new ArrayList<String>();
        Answer2 = new ArrayList<String>();
        Answer3 = new ArrayList<String>();
        MultipleAnswers1 = new ArrayList<String>();
        MultipleAnswers2 = new ArrayList<String>();
        MultipleAnswers3 = new ArrayList<String>();
        NotFoundAnswers = new ArrayList<String>();
        int count=0;

        WebDriverWait SearchBoxWait = new WebDriverWait(driver,30);
        SearchBoxWait.until(ExpectedConditions.elementToBeClickable(TxtboxSearch));

        //System.out.println("H4 size BEFORE" + Question.size());

        for(int i=0;i<Queries.size();i++) {

            TxtboxSearch.clear();
            TxtboxSearch.sendKeys(Query.get(i).toString());
            //TxtboxSearch.sendKeys(Keys.ENTER);
            BtnSearch.click();


            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }




           /* String winHandleBefore = driver.getWindowHandle();

            for (String winHandle : driver.getWindowHandles()) {
                driver.switchTo().window(winHandle);
            }*/
          /*  try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }*/

            if (Question.size() == 0) {

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                NotFoundAnswers.add("Result not Found");
               // BtnResultClose.get(2).click();


            } else {

                driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);

                if(!SingleAnswer.isEmpty() && !AnswerCountDiv.isEmpty())
                {


                    if(!PlayerDiv.isEmpty() && !PositionDiv.isEmpty()) // && !TeamDiv.isEmpty()
                    {

                        SingleResult.add(PlayerDiv.get(count).getText().replaceAll("WR","-") + PositionDiv.get(count).getText()+"-" +AnswerCountDiv.get(count).getText()); // "-" + TeamDiv.get(count).getText() +

                        //SingleResult.add(PlayerDiv.get(count).getText().replaceAll("WR","-") + "-" + PositionDiv.get(count).getText()+"-" +AnswerCountDiv.get(count).getText()); // "-" + TeamDiv.get(count).getText() +

                    }
                  /*  else if(!PlayerDiv.isEmpty() && !PositionDiv.isEmpty())
                    {

                        SingleResult.add(PlayerDiv.get(count).getText() + "-" + PositionDiv.get(count).getText()+ "-" + AnswerCountDiv.get(count).getText());
                    }
                    else if(!PlayerDiv.isEmpty() )//&& !TeamDiv.isEmpty()
                    {

                        SingleResult.add(PlayerDiv.get(count).getText() + "-" + AnswerCountDiv.get(count).getText()); //TeamDiv.get(count).getText() + "-" +
                    }*/

                    else
                    {

                        SingleResult.add("No Answer Found");
                    }


                }

                else
                {

                    Answer1 = new ArrayList<String>();
                    Answer2 = new ArrayList<String>();
                    Answer3 = new ArrayList<String>();


                    if(PositionDiv.isEmpty()) {


                        Answer1.add(PlayerDiv.get(0).getText().replaceAll("WR","").replaceAll("QB","").replaceAll("RB","").replaceAll("\\s+","")+"-");
                        //Answer1.add(PlayerDiv.get(0).getText().replaceAll("WR","").replaceAll("\\s+","")+"-");
                       // Answer1.add(TeamDivMultiple.get(0).getText().replaceAll("\\s+","")+"-");
                        Answer1.add(AnswerDivMultiple.get(0).getText().replaceAll("\\s+",""));

                        Answer2.add(PlayerDiv.get(1).getText().replaceAll("WR","").replaceAll("QB","").replaceAll("RB","").replaceAll("\\s+","")+"-");
                      //  Answer2.add(TeamDivMultiple.get(1).getText().replaceAll("\\s+","")+"-");
                        Answer2.add(AnswerDivMultiple.get(1).getText().replaceAll("\\s+",""));

                        Answer3.add(PlayerDiv.get(2).getText().replaceAll("WR","").replaceAll("QB","").replaceAll("RB","").replaceAll("\\s+","")+"-");
                      //  Answer3.add(TeamDivMultiple.get(2).getText().replaceAll("\\s+","")+"-");
                        Answer3.add(AnswerDivMultiple.get(2).getText().replaceAll("\\s+",""));


                        for(int c=0;c<Answer1.size();c++) {


                            MultipleData1=MultipleData1.concat(Answer1.get(c).toString());
                            //MultipleData1= MultipleData1.substring(0,MultipleData1.length()-1);


                        }
                        MultipleAnswers1.add(MultipleData1);

                        for(int d=0;d<Answer2.size();d++)
                        {

                            MultipleData2 = MultipleData2.concat(Answer2.get(d).toString());
                            //MultipleData2= MultipleData1.substring(0,MultipleData1.length()-1);

                        }
                        MultipleAnswers2.add(MultipleData2);

                        for(int e=0;e<Answer3.size();e++)
                        {

                            MultipleData3 = MultipleData3.concat(Answer3.get(e).toString());
                            //MultipleData1= MultipleData3.substring(0,MultipleData1.length()-1);

                        }
                        MultipleAnswers3.add(MultipleData3);



                    }

                }


              /*  WebDriverWait CloseButtonWait = new WebDriverWait(driver,30);
                CloseButtonWait.until(ExpectedConditions.visibilityOf(BtnResultClose.get(2)));

                BtnResultClose.get(2).click();*/
            }

            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            UtilityTest.WriteQueriesRows("Which");
            SingleResult.clear();
            NotFoundAnswers.clear();
            MultipleData1 ="";
            MultipleData2 ="";
            MultipleData3 ="";
            MultipleAnswers1.clear();
            MultipleAnswers2.clear();
            MultipleAnswers3.clear();

        }

        return this;
    }

    public SearchPage CompareResult(String Sheet)
    {

        UtilityTest.CompareData(Sheet);
        return this;
    }


}
