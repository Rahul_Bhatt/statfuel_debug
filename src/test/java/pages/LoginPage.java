package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by Rahul on 8/4/2016.
 */
public class LoginPage {
    protected WebDriver driver;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
    }

    @FindBy(xpath = "//a[text()='Sign in/up']")
    private WebElement BtnSignin;

    @FindBy(xpath = "//i[@class='fa fa-envelope-o']")
    private WebElement BtnSigninWithEmail;

    @FindBy(id = "user_name")
    private WebElement TxtboxUserName;

    @FindBy(id = "password_login")
    private WebElement TxtboxPassword;

    @FindBy(id = "log-in-button")
    private WebElement BtnLogin;

    public LoginPage Login(String Uname, String Pass)
    {


        WebDriverWait SigninBtnWait = new WebDriverWait(driver , 120);
        SigninBtnWait.until(ExpectedConditions.elementToBeClickable(BtnSignin));

        BtnSignin.click();

        String winHandleBefore = driver.getWindowHandle();

        for(String winHandle : driver.getWindowHandles()){
            driver.switchTo().window(winHandle);
        }


        WebDriverWait SigninEmailBtnWait = new WebDriverWait(driver , 120);
        SigninEmailBtnWait.until(ExpectedConditions.elementToBeClickable(BtnSigninWithEmail));

        BtnSigninWithEmail.click();
        TxtboxUserName.sendKeys(Uname);
        TxtboxPassword.sendKeys(Pass);

        BtnLogin.click();
        return this;
    }




}

