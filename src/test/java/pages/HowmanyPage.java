package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import testcases.UtilityTest;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static testcases.UtilityTest.Queries;

/**
 * Created by admin on 09-08-2016.
 */
public class HowmanyPage {



    protected WebDriver driver;

    public HowmanyPage (WebDriver driver) {
        this.driver = driver;
    }

    @FindBy(id = "stat-search-lgd")
    private WebElement TxtboxSearch;

    @FindBy(id = "statefulsearch-form-guest")
    private WebElement BtnSearch;

    @FindBy(xpath = "//p[text()='No results found for the given criteria']")
    private List<WebElement> SectionNoResultFound;

    @FindBy(xpath = "//i[@class='fa fa-times']")
    private List<WebElement> BtnResultClose;

    @FindBy(tagName = "h4")
    private List<WebElement> Question;

    @FindBy(xpath = "//div[@class='row sa-ans-row']")
    private List<WebElement> AnswersSize;

    @FindBy(xpath = "//div[@class='wrap-ans single-ans']")
    private List<WebElement> SingleAnswer;

    @FindBy(xpath = "//span[@class='player']")
    private List<WebElement> PlayerDiv;

    @FindBy(xpath = "//span[@class='position']")
    private List<WebElement> PositionDiv;

    @FindBy(xpath = "//p[@class='team']")
    private List<WebElement> TeamDiv;

    @FindBy(xpath = "//p[@class='sc-score']")
    private List<WebElement> SCScore;

    @FindBy(xpath = "//span[@class='team']")
    private List<WebElement> TeamDivMultiple;

    @FindBy(xpath = "//p[@class='ans']")
    private List<WebElement> AnswerCountDiv;

    @FindBy(xpath = "//p[@class='ans superlative']")
    private List<WebElement> AnswerDivMultiple;


    public static List<String> HNotFoundAnswers;
    public static List<String> HSingleResult;
    public static List<String> HAnswer1;
    public static List<String> HAnswer2;
    public static List<String> HAnswer3;
    public static List<String> HMultipleAnswers1;
    public static List<String> HMultipleAnswers2;
    public static List<String> HMultipleAnswers3;
    String HMultipleData1="";
    String HMultipleData2="";
    String HMultipleData3="";



    public HowmanyPage InputWhichTypeQuery(List<String> Query)
    {

        HSingleResult = new ArrayList<String>();
        HAnswer1 = new ArrayList<String>();
        HAnswer2 = new ArrayList<String>();
        HAnswer3 = new ArrayList<String>();
        HMultipleAnswers1 = new ArrayList<String>();
        HMultipleAnswers2 = new ArrayList<String>();
        HMultipleAnswers3 = new ArrayList<String>();
        HNotFoundAnswers = new ArrayList<String>();
        int count=0;

        WebDriverWait SearchBoxWait = new WebDriverWait(driver,30);
        SearchBoxWait.until(ExpectedConditions.elementToBeClickable(TxtboxSearch));

        //System.out.println("H4 size BEFORE" + Question.size());

        for(int i=0;i<Queries.size();i++) {

            TxtboxSearch.clear();
            TxtboxSearch.sendKeys(Query.get(i).toString());
            //TxtboxSearch.sendKeys(Keys.ENTER);
            BtnSearch.click();


            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }




           /* String winHandleBefore = driver.getWindowHandle();

            for (String winHandle : driver.getWindowHandles()) {
                driver.switchTo().window(winHandle);
            }*/
          /*  try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }*/

            if (Question.size() == 0) {

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                HNotFoundAnswers.add("Result not Found");
                // BtnResultClose.get(2).click();


            } else {

                driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);

                if(!SingleAnswer.isEmpty() && !AnswerCountDiv.isEmpty() || !SCScore.isEmpty())
                {


                    if(!PlayerDiv.isEmpty() && !PositionDiv.isEmpty()) // && !TeamDiv.isEmpty()
                    {

                        HSingleResult.add(PlayerDiv.get(count).getText()+"-" +AnswerCountDiv.get(count).getText()); // "-" + TeamDiv.get(count).getText() +

                        //SingleResult.add(PlayerDiv.get(count).getText().replaceAll("WR","-") + "-" + PositionDiv.get(count).getText()+"-" +AnswerCountDiv.get(count).getText()); // "-" + TeamDiv.get(count).getText() +

                    }
                    else if(SCScore.size()>0)
                    {

                        HSingleResult.add(SCScore.get(count).getText());
                    }
                  /*  else if(!PlayerDiv.isEmpty() && !PositionDiv.isEmpty())
                    {

                        SingleResult.add(PlayerDiv.get(count).getText() + "-" + PositionDiv.get(count).getText()+ "-" + AnswerCountDiv.get(count).getText());
                    }
                    else if(!PlayerDiv.isEmpty() )//&& !TeamDiv.isEmpty()
                    {

                        SingleResult.add(PlayerDiv.get(count).getText() + "-" + AnswerCountDiv.get(count).getText()); //TeamDiv.get(count).getText() + "-" +
                    }*/

                    else
                    {

                        HSingleResult.add("No Answer Found");
                    }


                }

                else
                {

                    HAnswer1 = new ArrayList<String>();
                    HAnswer2 = new ArrayList<String>();
                    HAnswer3 = new ArrayList<String>();


                    if(PositionDiv.isEmpty()) {


                        HAnswer1.add(PlayerDiv.get(0).getText().replaceAll("WR","").replaceAll("QB","").replaceAll("\\s+","")+"-");
                        //Answer1.add(PlayerDiv.get(0).getText().replaceAll("WR","").replaceAll("\\s+","")+"-");
                        // Answer1.add(TeamDivMultiple.get(0).getText().replaceAll("\\s+","")+"-");
                        HAnswer1.add(AnswerDivMultiple.get(0).getText().replaceAll("\\s+",""));

                        HAnswer2.add(PlayerDiv.get(1).getText().replaceAll("WR","").replaceAll("QB","").replaceAll("\\s+","")+"-");
                        //  Answer2.add(TeamDivMultiple.get(1).getText().replaceAll("\\s+","")+"-");
                        HAnswer2.add(AnswerDivMultiple.get(1).getText().replaceAll("\\s+",""));

                        HAnswer3.add(PlayerDiv.get(2).getText().replaceAll("WR","").replaceAll("QB","").replaceAll("\\s+","")+"-");
                        //  Answer3.add(TeamDivMultiple.get(2).getText().replaceAll("\\s+","")+"-");
                        HAnswer3.add(AnswerDivMultiple.get(2).getText().replaceAll("\\s+",""));


                        for(int c=0;c<HAnswer1.size();c++) {


                            HMultipleData1=HMultipleData1.concat(HAnswer1.get(c).toString());
                            //MultipleData1= MultipleData1.substring(0,MultipleData1.length()-1);


                        }
                        HMultipleAnswers1.add(HMultipleData1);

                        for(int d=0;d<HAnswer2.size();d++)
                        {

                            HMultipleData2 = HMultipleData2.concat(HAnswer2.get(d).toString());
                            //MultipleData2= MultipleData1.substring(0,MultipleData1.length()-1);

                        }
                        HMultipleAnswers2.add(HMultipleData2);

                        for(int e=0;e<HAnswer3.size();e++)
                        {

                            HMultipleData3 = HMultipleData3.concat(HAnswer3.get(e).toString());
                            //MultipleData1= MultipleData3.substring(0,MultipleData1.length()-1);

                        }
                        HMultipleAnswers3.add(HMultipleData3);



                    }

                }


              /*  WebDriverWait CloseButtonWait = new WebDriverWait(driver,30);
                CloseButtonWait.until(ExpectedConditions.visibilityOf(BtnResultClose.get(2)));

                BtnResultClose.get(2).click();*/
            }

            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            UtilityTest.WriteQueriesRowsHowMany("HowMany");
            HSingleResult.clear();
            HNotFoundAnswers.clear();
            HMultipleData1 ="";
            HMultipleData2 ="";
            HMultipleData3 ="";
            HMultipleAnswers1.clear();
            HMultipleAnswers2.clear();
            HMultipleAnswers3.clear();

        }

        return this;
    }

    public HowmanyPage CompareResult(String Sheet)
    {

        UtilityTest.CompareData(Sheet);
        return this;
    }
}
